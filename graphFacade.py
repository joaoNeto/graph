import os
from Entities.graph import Graph
import sys

class GraphFacade:

    def __init__(self):
        self.graph = Graph()

    def insert_node(self):
        os.system('cls||clear')
        print("----------------------------------------- INSERT NODE ")
        node_name = input("Input the name of node.: ")
        self.graph.set_node(node_name)

    def insert_edge(self):
        os.system('cls||clear')

        double_relationship = "-"
        vector_relationship = "->"
        chose_relationship = double_relationship

        print("----------------------------------------- INSERT EDGE ")
        print("PS.: To insert the edge you need input the two nodes separated by '-' or '->', ex.: node_one-node_two, node_one->node_two, etc. ")
        edge = input("Input the relationship .: ")
        weight = input("Input the value of the above edge.: ")

        if vector_relationship not in edge and double_relationship not in edge:
            raise Exception("You don't inform the relationship correctly, ex.: node_one-node_two, node_one->node_two, etc.")
        
        if vector_relationship in edge:
            chose_relationship = vector_relationship

        nodes, graph = self.graph.get_graph()
        node_relationship = edge.split(chose_relationship)

        if len(node_relationship) != 2:
            raise Exception("Check you passed the params correctly, ex.: node_one-node_two, node_one->node_two, etc.")

        if node_relationship[0] not in nodes and node_relationship[1] not in nodes:
            raise Exception("Sorry, but one of those nodes doesn't exists, check if you was passed the params correctly!")

        relationship = "double_relationship" if chose_relationship == double_relationship else "vector_relationship"

        self.graph.set_edge(
            node_from=node_relationship[0], 
            node_to=node_relationship[1],
            weight=weight,
            type=relationship
        )
        print("Successfull!!")
        input() 

    def show_matrix(self):
        os.system('cls||clear')
        print("----------------------------------------- SHOW MATRIX GRAPH ")
        nodes, graph = self.graph.get_graph()
        text  = " - | "
        text += ' , '.join((str(node) for node in nodes ))
        text += "\n"

        for i_line, line in enumerate(graph):
            text += " "+nodes[i_line]+" | "
            text += ' , '.join((str(value) for value in line ))
            text += "\n"

        print(text)
        input()

    def show_tree(self):
        os.system('cls||clear')
        print("----------------------------------------- SHOW TREE GRAPH")
        print(self.graph.get_tree_graph())
        input()

    def search(self):
        DFS_OPTION = 1
        DIJKSTRA_OPTION = 2

        os.system('cls||clear')
        print("----------------------------------------- SEARCH ")
        print(f"({DFS_OPTION}) DFS")
        print(f"({DIJKSTRA_OPTION}) dijkstra")
        option = int(input("choose yout option.: "))

        os.system('cls||clear')
        print("----------------------------------------- SEARCH ")
        node_from = input("Which node do you want to start from? ")
        node_to = input("Which node do you want to start to? ")

        if option == DFS_OPTION:
            nodes_found = self.depth_first_search(node_from, node_to)
        elif option == DIJKSTRA_OPTION:
            nodes_found = self.dijkstra(node_from, node_to)
        else:
            raise Exception("Ops! i don't undestand what do you want to choose :(")

        print("\n")
        print(" -> ".join( (str(node) for node in nodes_found) ))
        print("\n")
        input()


    def depth_first_search(self, node_from, node_to): 
        """ Depth-first search (DFS), is an algorithm for tree traversal on graph or tree data structures """

        def next_node(graph, node_to, stack):
            if len(stack) == 0:
                raise Exception("Error searching the nodes, please check if you input the nodes correctly!")

            for node in graph[stack[-1]]:

                if node['value'] == node_to:
                    stack.append(node['value'])
                    return stack

                if not node['visited']:
                    stack.append(node['value'])
                    node['visited'] = True
                    return next_node(graph, node_to, stack)

            stack.pop()
            return next_node(graph, node_to, stack)

        graph = self.graph.get_tree_graph()

        stack = [node_from]
        new_graph = {}

        for key in graph:
            new_graph[key] = [ {'value': node, 'visited': False} for node in graph[key]]

        for node in new_graph[stack[-1]]:
            stack.append(node['value'])
            node['visited'] = True

            if node['value'] == node_to:
                break

            return next_node(new_graph, node_to, stack)

        return stack


    def dijkstra(self, node_from, node_to): 

        graph = self.graph.get_tree_graph()

        current_node = node_from
        distances    = {key:{'distance': float('inf'), 'prev_node': False} for key in graph.keys()}
        sum_weight_nodes = { }
        nodes_not_visiteds = [node for node in graph.keys()]

        distances[current_node] = {'distance': 0, 'prev_node': node_from}

        nodes_not_visiteds.remove(current_node)

        while nodes_not_visiteds:
            for node_neighbor, weight_neighbor in graph[current_node].items():
                distance_from_origin = weight_neighbor + distances[current_node]['distance']
                if distances[node_neighbor]['distance'] == float("inf") or distance_from_origin < distances[node_neighbor]['distance']:
                    distances[node_neighbor] = {'distance': distance_from_origin, 'prev_node': current_node}
                    sum_weight_nodes[node_neighbor] = distance_from_origin

            if sum_weight_nodes == {} : break    
            node_less_weight, weight = min(sum_weight_nodes.items(), key=lambda x: x[1]) # Get the key corresponding to the minimum value within a dictionary
            current_node = node_less_weight
            nodes_not_visiteds.remove(current_node) # remove the less node and analyse the others nodes
            del sum_weight_nodes[current_node]

        route = [node_to]

        def handle_route(distances, node_from, route):
            node_prev = distances[route[0]]['prev_node']
            route.insert(0, node_prev)

            if node_from == node_prev:
                return route

            return handle_route(distances, node_from, route)

        handle_route(distances, node_from, route)
        # distances[node_to]['distance'] => distance between origin and target node
        return route
