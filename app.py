import os
from graphFacade import GraphFacade
from enum import Enum

graph_facade = GraphFacade()

class ChooseMenu(Enum):
    INSERT_NODE = 1
    INSERT_EDGE = 2
    SHOW = 3
    SEARCH = 4
    EXIT = 5

while True:
    try:
        os.system('cls||clear')
        print("----------------------------------------- GRAPH PROGRAM ")
        print("(1) Insert one node in graph")
        print("(2) Insert one edge in graph")
        print("(3) Show graph")
        print("(4) Search ")
        print("(5) Exit of program")
        i_chose = input("choose your option.: ")
        chose = ChooseMenu(int(i_chose)).name

        if chose == "INSERT_NODE":
            graph_facade.insert_node()
        elif chose == "INSERT_EDGE":        
            graph_facade.insert_edge()
        elif chose == "SHOW":
            print("----------------------------------------- SHOW GRAPH ")
            print("(1) Matrix")
            print("(2) Tree")
            option = int(input("choose yout option.: "))
            if option == 1:
                graph_facade.show_matrix()
            elif option == 2:
                graph_facade.show_tree()
            else:
                raise Exception("Sorry, you don't chose correctly")
        elif chose == "SEARCH":
            graph_facade.search()
        elif chose == "EXIT":
            os.system('cls||clear')
            break
        else:
            print("Ops! i don't undestand what do you want to choose :(")
            input()
    except Exception as error:
        print("APPLICATION ERROR!!!!")
        print(str(error))
        input()

print("FINISHED!!!")