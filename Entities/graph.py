
from Entities.node import Node

class Graph:

    def __init__(self):
        self.__graph = []
        self.__node = Node()

    def set_node(self, node_name):
        self.__node.set_node(node_name)

        if(self.__graph == []):
            self.__graph.append([0])
        else:
            # add more one column in graph matrix
            for i, nodes in enumerate(self.__graph):
                nodes.append(0)
                self.__graph[i] = nodes

            # add more one line on graph matrix
            self.__graph.append([ 0 for i in range(len(self.__node.get_node()))]) 

    def set_edge(self, node_from, node_to, weight=1, type="double_relationship"):
        nodes = self.__node.get_node()

        i = nodes.index(node_from)
        j = nodes.index(node_to)

        self.__graph[i][j] = weight
        if type == "double_relationship":
            self.__graph[j][i] = weight

    def get_graph(self):
        return (self.__node.get_node(), self.__graph)

    def get_tree_graph(self):
        nodes = self.__node.get_node()
        graph = self.__graph
        tree = {}

        for index, value in enumerate(nodes):  
            tree[value] = {str(nodes[i]): int(v) for i, v in enumerate(graph[index]) if int(v) > 0}

        return tree