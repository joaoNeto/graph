# GRAPHS

![Image of a graph](https://gitlab.c3sl.ufpr.br/uploads/-/system/project/avatar/3174/molecule.png?width=48)

[[_TOC_]]

to run execute the project it's very easy, you just need run this command in your terminal.

```bash
python3 app.py
```

## About graphs

A Graph is a non-linear data structure consisting of nodes and edges. The nodes are sometimes also referred to as vertices and the edges are lines or arcs that connect any two nodes in the graph. 

Graphs are used to solve many real-life problems. Graphs are used to represent networks. The networks may include paths in a city or telephone network or circuit network. Graphs are also used in social networks like linkedIn, Facebook. For example, in Facebook, each person is represented with a vertex(or node). Each node is a structure and contains information like person id, name, gender, locale etc.

### Representing a graph

#### Edge list

A simple form to represent a graph it's like a list of list, the name is edge lists where each list there are two positions and each position will have the node value.

    [ 
        [0,1], # an edge from node 0 to 1
        [0,3], 
        [0,4], 
        [1,2], 
        [1,3]
    ]

the graph above represent a graph with 5 nodes (0,1,2,3,4) and 5 edges (count of lists)

#### Adjacency matrix

adjacency matrix is a quadratic and sparse matrix where the number of columns and line represent the same node values, soon, each column and each represent each node. for being a sparse matrix the most values will be zero and the another values represent an relationship between one node to other. let's see an example using the graph representation above.



    [
        [0,1,0,1,1],
        [0,0,1,1,0],
        [0,0,0,0,0],
        [0,0,0,0,0],
        [0,0,0,0,0]
    ]


PS.: Use the adjacency matrix if the graph will be dense.

#### Adjacency list

Another way to represent an graph is using a adjacency list, represent the union of adjacency matrix with the edge list. The adjacency list is a list of list where each index of the  manly list represent one node and the list inside of list represent all the nodes connected with the index node, see the example below.


    [
        [1,3,4], # node 0 have edges with the nodes 1,2 and 4
        [2,3],
        [],
        [],
        []
    ]


PS.: It's very important say, if you need create or manipulate a graph in a simple and fast way, the representation above is the best option.


### Search algorithms

One of the best things to work with graphs is search nodes in a graphs, let's see some examples of search algorithms bellow.

#### Breadth first search (BFS)

![Image of a graph](https://raw.githubusercontent.com/kdn251/interviews/master/images/dfsbfs.gif)

The special property is that the tree has no cycles: given any two vertices, there is exactly 1 path between them. An extension route is to visit each node starting at the lowest level moves to the highest levels level after level, visiting each node from left to right. Its implementation is straightforward when a queue is used. After a node is visited, its children, if any, are placed at the end of the queue and the node at the beginning of the queue is visited. Thus, nodes at level n + 1 will be visited only after having visited all nodes at level n. Compute the shortest distance to all vertices attainable. The sub-graph containing the paths taken is called the breadth-first tree.

#### Depth first search (DFS)

![Image of a graph](https://raw.githubusercontent.com/kdn251/interviews/master/images/dfsbfs.gif)

An in-depth search algorithm performs an uninformed search that progresses through the expansion of the first child node in the search tree, and deepens further, until the search target is found or until it comes across a node that has no children (leaf node). Then the search goes back (backtrack) and starts at the next node. In a non-recursive implementation, all newly expanded nodes are added to a stack to perform the exploration. The spatial complexity of a depth search algorithm is much less than that of a width search algorithm. The temporal complexity of both algorithms is proportional to the number of vertices added to the number of edges of the graphs to which they cross. When searching in very large graphs, which cannot be stored completely in memory, the depth search does not end, in cases where the length of a path in a search tree is infinite. The simple trick of "remembering which nodes have been visited" does not work, because there may not be enough memory. This can be resolved by setting a limit to increase the depth of the tree.

#### Dijkstra

The Dijkstra algorithm, designed by the Dutch computer scientist Edsger Dijkstra in 1956 and published in 1959, solves the shortest path problem in a directed or non-directed graph with non-negative weight edges, in computational time O (m + n log n ) where m is the number of edges and n is the number of vertices. The algorithm that serves to solve the same problem in a graph with negative weights is the Bellman-Ford algorithm, which has a longer execution time than Dijkstra.

## References

- https://www.youtube.com/watch?v=gdmfOwyQlcI
- https://www.youtube.com/watch?v=MC0u4f334mI
- https://github.com/kdn251/interviews#graph-algorithms

